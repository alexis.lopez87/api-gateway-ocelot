using Microsoft.OpenApi.Models;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

var env = builder.Environment.EnvironmentName;

if (builder.Environment.IsDevelopment())
{
    env = "";
} else
{
    env = "." + env;
}

builder.Configuration.AddJsonFile($"ocelot{env}.json", optional: false, reloadOnChange: true);

builder.Services.AddOcelot();

var app = builder.Build();

app.UseOcelot().Wait();

app.Run();
